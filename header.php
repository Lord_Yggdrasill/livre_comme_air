<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <script type="application/javascript" src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script type="application/javascript" src="vendor/jquery/popper.min.js"></script>
    <script type="application/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <title>Livre comme l'air</title>
</head>
<body>
