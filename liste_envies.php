<?php
session_start();
require 'header.php';
require 'nav.php';

//connexion à) la bdd
try {
    $bdd = new PDO('mysql:host=localhost;dbname=livre_comme_air;charset=utf8;', 'root', '',
        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}

catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}



//foreach ($_POST['livre'] as $valeur) {
//    echo 'Livre(s) sélectionné(s) : ' . $valeur .'<br>';
//}

if (isset($_SESSION['pseudo']) && !empty($_SESSION['books'])) {

    $req = $bdd->query('SELECT * FROM books WHERE id IN (' . implode(',', array_map('intval', $_SESSION['books'])) . ')' )
    ?>
    <div class="flex-mode">
<?php

while ($donnees = $req->fetch()) {?>
    <div class="fetch-books">
    <a href="favorites_books.php?id_remove=<?php echo $donnees['id']?>">
        <img src="img_livres/<?php echo $donnees['img']?>" alt="Livre" id="<?php echo $donnees['id']?>" class="img-book">
    </a>
    <?php
    echo '<div class="info-books">Titre : ' . $donnees['title']. '<br>
    Prix : ' . $donnees['price'] . '<br>
    Description : ' . $donnees['description'] . '</div>';
    if (isset($_SESSION['pseudo'])) { //A REVOIR DEMAIN
        $_SESSION['livres'] = array();
        ?>
<!--        <label for="livre">Ajouter ce livre </label>-->
<!--        <input type="checkbox" name="livre[]" value="--><?php //echo $donnees['title']?><!--" form="check-books">-->
    <?php
    }?></div><?php
}

?>
<!--    <form action="liste_envies.php" method="post" id="check-books">-->
<!--        <input type="submit" name="validate" value="J'ajoute ces livres !">-->
<!--    </form>-->
<?php

$req->closeCursor();
?>
</div>
<?php
} elseif (!isset($_SESSION['pseudo'])) {
    echo 'Vous devez être connecté afin de gérer votre liste de livres. <br>';
          echo '<a href="connexion.php">Se connecter</a>';
}

?>

