<?php
session_start();
if (empty($_SESSION['books'])) {
    $_SESSION['books'] = [];
}
require 'header.php';


//connexion à) la bdd
try {
    $bdd = new PDO('mysql:host=localhost;dbname=livre_comme_air;charset=utf8;', 'root', '',
        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}

catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}


//Si les POST pseudo et pw existent :
if (isset($_POST['pseudo']) && isset($_POST['pw'])) {
    //si les champs ne sont pas vides
    if (!empty($_POST['pseudo'] && !empty($_POST['pw']))) {
        //je prépare une requête pour récupérer le pseudo posté, et le comparer à la bdd user
        $req = $bdd->prepare('SELECT id, login, password FROM users WHERE login = :pseudo');
        $req->execute(array(
            'pseudo' => $_POST['pseudo']
        ));

        //je parcours la bdd que et je met la requête dans la variable resultat
        $resultat = $req->fetch();


        //s'il n'y a pas la variable résultat on affiche un message
        if (!$resultat) {
            echo 'Mauvais identifiant ou mot de passe';
            //on hashe le mdp posté, et s'il correspond à celui de la bdd,
            //on stocke l'id et le pseudo dans la SESSION
        } elseif (hash('sha512', $_POST['pw']) === $resultat['password']) {
            $_SESSION['id'] = $resultat['id'];
            $_SESSION['pseudo'] = $_POST['pseudo'];
            echo 'Bienvenue ' . strip_tags($_SESSION['pseudo']);
        } else {
            echo 'Mauvais  mot de passe';
        }
    } else {
        echo 'Veuillez renseigner tous les champs';
    }
}

//si la case 'rester connecté" est cochée, je set les cookies pour le login et le mdp
if (isset($_POST['perma_co'])) {
    setcookie('login', $_POST['pseudo'], time() + 3600*24*365, null, null, false, true); //cookie qui sotcke le pseudo
    setcookie('pass_hash', hash('sha512', $_POST['pw']), time() + 3600*24*365, null, null, false, true); //cookie qui stocke le mdp hashé
}

/*if (isset($_COOKIE['login']) && isset($_COOKIE['pass_hash'])) {


    if ($_COOKIE['login'] == $resultat['login'] && $_COOKIE['pass_hash'] == $resultat['password']) {
        $_SESSION['id'] = $resultat['id'];
        $_SESSION['pseudo'] = $_POST['pseudo'];
    }
}*/


require 'nav.php';

//requête qui prend les champs de la table books
$req = $bdd->query('SELECT * FROM books');
?>

<div class="flex-mode">
<?php
//tant qu'il y a des entrées dans la table, on les affiche
while ($donnees = $req->fetch()) {?>
    <div class="fetch-books">
<!--    lien a href qui va sur la page favorites_books en envoyant un parametre GET id_book dont la valeur est l'id
qui je vais chercher dans la bdd. -->
    <a href="favorites_books.php?id_book=<?php echo $donnees['id']?>">
        <img src="img_livres/<?php echo $donnees['img']?>" alt="Livre" id="<?php echo $donnees['id']?>" class="img-book">
    </a>
    <?php
    echo '<div class="info-books">Titre : ' . $donnees['title']. '<br>
    Prix : ' . $donnees['price'] . '<br>
    Description : ' . $donnees['description'] . '</div>';
    if (isset($_SESSION['pseudo'])) { //A REVOIR DEMAIN
        $_SESSION['livres'] = array();
        ?>
<!--        <label for="livre">Ajouter ce livre </label>-->
<!--        <input type="checkbox" name="livre[]" value="--><?php //echo $donnees['title']?><!--" form="check-books">-->
    <?php
    }?></div><?php
}

?>
<!--    <form action="liste_envies.php" method="post" id="check-books">-->
<!--        <input type="submit" name="validate" value="J'ajoute ces livres !">-->
<!--    </form>-->
<?php

$req->closeCursor();
?>
</div>



<?php
require 'footer.php';
?>
