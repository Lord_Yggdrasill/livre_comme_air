<?php
session_start();

//Si je reçois l'id "id_book" du livre en GET, je l'insère dans un array de la SESSION
if (isset($_GET['id_book'])) {
    //je cherche si l'id qu'on m'envoie en GET existe déjà dans la SESSION, je mets ça dans une variable
    $duplicated_book = array_search($_GET['id_book'], $_SESSION['books']);
    //si la variable est fausse, alors je peux push l'id dans l'array
    if (!$duplicated_book) {
        array_push($_SESSION['books'], $_GET['id_book']) ;
    }

}

//Si je reçois l'id "id_remove" du livre en GET, je l'enlève de  l'array de la SESSION
if (isset($_GET['id_remove'])) {
    $book_to_remove = array_search($_GET['id_remove'], $_SESSION['books']);
    unset($_SESSION['books'][$book_to_remove]);

}

//redirection
header('Location: liste_envies.php');